import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import { storage } from '../config/firebase';

import { storage } from '../config/firebase';

export const uploadProfileImg = async (fileObject) => {
  const imgRef = storageRef(storage, `profile_img/${fileObject.name}`);
  const snapshot = await uploadBytes(imgRef, fileObject);
  const url = await getDownloadURL(imgRef);
  // console.log(url);
  return url;
};

export const uploadImgCloudinary = async (fileObject) => {
  const formData = new FormData();
  formData.append('file', fileObject);
  formData.append('upload_preset', 'binar-platinum');

  // POST https://api.cloudinary.com/v1_1/dcwfurolv/image/upload
  const resp = await fetch(
    'https://api.cloudinary.com/v1_1/dx4buq9vw/image/upload',
    {
      method: 'POST',
      body: formData,
    },
  );
  const data = await resp.json();
  return data.url;
};

export const uploadVideoCloudinary = async (fileObject) => {
  const formData = new FormData();
  formData.append('file', fileObject);
  formData.append('upload_preset', 'binar-platinum');

  // POST https://api.cloudinary.com/v1_1/dcwfurolv/image/upload
  const resp = await fetch(
    'https://api.cloudinary.com/v1_1/dx4buq9vw/video/upload',
    {
      method: 'POST',
      body: formData,
    },
  );
  const data = await resp.json();
  return data.url;
};
