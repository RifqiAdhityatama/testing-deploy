import React, { useState, useEffect } from 'react';

import { Card, Table } from 'react-bootstrap';
import { retrieveAllGames } from '../../actions/games';
import stylesProfileGameHistory from './ProfileGameHistory.module.css';

function ProfileGameHistory({ userGameHistory }) {
  const [gameUserById, setGameUserById] = useState({});
  useEffect(() => {
    async function initData() {
      const obj = {};
      const allGames = await retrieveAllGames();

      allGames.forEach((val) => {
        obj[val.id] = val.data.game_title;
      });

      setGameUserById(obj);
    }
    initData();
  }, []);

  return (
    <section className={stylesProfileGameHistory.sectionProfileGameHistory}>
      <Card
        style={{
          backgroundColor: '#3B3838',
          boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
          height: '100vh',
        }}
      >
        {/* ProfileGameHistory Top */}
        <div style={{ backgroundColor: '#464343' }}>
          <Card.Header
            className={stylesProfileGameHistory.profileGameHistoryHeader}
          >
            GAME HISTORY
          </Card.Header>
        </div>

        {/* ProfileGameHistory Bottom */}
        <Card.Body>
          <Table bordered variant="dark">
            <thead className="text-white">
              <tr>
                <th>No</th>
                <th>Game</th>
                <th>Game Time</th>
                <th>Score</th>
              </tr>
            </thead>
            <tbody className="text-white">
              {userGameHistory?.map((user, i) => (
                <tr key={user?.data?.id_player}>
                  <td>{i + 1}</td>
                  <td>{gameUserById[user?.data?.game_id]}</td>
                  <td>{user?.data?.time}</td>
                  <td>{user?.data?.score}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    </section>
  );
}

export default ProfileGameHistory;
