import { updateProfile } from 'firebase/auth';
import {
  insertGameCard,
  insertSlideshow,
  insertVideo,
  registerUser,
  retrieveAllUser,
  updatePlayerRank,
  updateProfileImg,
  updateScore,
  updateTotalGame,
} from '../../actions/fb_database';

// reigster user
describe('register user', () => {
  jest.mock('firebase/database', () => ({
    set: () => {},
    ref: () => {},
    getDatabase: () => {},
  }));

  test('registerUser: successfully', async () => {
    const playerId = 'sKGgzDWf5CcJ3T8gTzzTNOHmEeE3';
    const userData = {
      name: 'programmerlyfe',
      username: 'programmer',
      email: 'programmer@gmail.com',
    };
    const data = await registerUser(
      playerId,
      userData.name,
      userData.username,
      userData.email,
    );
    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// edit profile
describe('edit profile', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    update: () => {},
    getDatabase: () => {},
  }));

  test('editProfile: successfully', async () => {
    const playerId = 'sKGgzDWf5CcJ3T8gTzzTNOHmEeE3';

    const userData = {
      name: 'programmerGoogle',
      username: 'programmergo',
      city: 'new york',
      social_media: 'programmerGoogle',
      profile_picture:
        'https://mir-s3-cdn-cf.behance.net/project_modules/fs/e1fd5442419075.57cc3f77ed8c7.png',
    };
    const data = await updateProfile(
      playerId,
      userData.name,
      userData.username,
      userData.city,
      userData.social_media,
      userData.profile_picture,
    );
    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// update score
describe('update score', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    update: () => {},
    getDatabase: () => {},
  }));

  test('update score: successfully', async () => {
    const playerId = 'sKGgzDWf5CcJ3T8gTzzTNOHmEeE3';
    const totalScore = 1;

    const data = await updateScore(playerId, totalScore);

    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// update totalGame
describe('update total game', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    update: () => {},
    getDatabase: () => {},
  }));

  test('update total game: successfully', async () => {
    const playerId = 'sKGgzDWf5CcJ3T8gTzzTNOHmEeE3';

    const totalGame = 10;

    const data = await updateTotalGame(playerId, totalGame);

    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// update playerRank
describe('update player rank', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    update: () => {},
    getDatabase: () => {},
  }));

  test('update player rank: successfully', async () => {
    const playerId = 'sKGgzDWf5CcJ3T8gTzzTNOHmEeE3';
    const playedGame = 'fortnite';
    const data = await updatePlayerRank(playerId, playedGame);

    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// insert gameCard
describe('insert game card', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    push: () => {},
    getDatabase: () => {},
  }));

  test('insert game: successfully', async () => {
    const playerId = 'sKGgzDWf5CcJ3T8gTzzTNOHmEeE3';

    const gameData = {
      gameTitle: 'fortnite',
      gameDescription:
        'online video game developed by Epic Games and released in 2017',
      gameImage:
        'https://images.wallpapersden.com/image/download/dwayne-johnson-the-rock-fortnite_bWVuammUmZqaraWkpJRmbmdlrWZlbWU.jpg',
      gameUrl: 'game/fortnite',
    };

    const data = await insertGameCard(
      gameData.gameTitle,
      gameData.gameDescription,
      gameData.gameImage,
      gameData.gameUrl,
    );

    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// insertSlideHomeShow
describe('insert slide show', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    push: () => {},
    getDatabase: () => {},
  }));

  test('insert slide show: successfully', async () => {
    const slideShowData = {
      hsImage:
        'https://images.unsplash.com/44/fN6hZMWqRHuFET5YoApH_StBalmainCoffee.jpg?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
      hsTitle: 'coffeVibes ',
    };

    const data = await insertSlideshow(
      slideShowData.hsImage,
      slideShowData.hsTitle,
    );
    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});

// insertHomeVideo
describe('insert video', () => {
  jest.mock('firebase/database', () => ({
    ref: () => {},
    set: () => {},
    getDatabase: () => {},
  }));

  test('insert video: successfully', async () => {
    const videoData = {
      urlVideo:
        'http://res.cloudinary.com/dx4buq9vw/video/upload/v1671355898/assets/cjaznvuwseuxe2b4rcxg.mp4',
      videoTitle: 'intro video',
    };

    const data = await insertVideo(videoData.urlVideo, videoData.videoTitle);

    expect(data).toEqual(undefined);
    expect(data).not.toBe(null);
  });
});
