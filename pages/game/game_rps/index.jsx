import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import Image from 'next/image';
import Navbar from '../../../components/Layout/Nav/Navbar';
import imgHandBatu from '../../../public/assets/games/rock-paper-scissors/hand_batu_old.png';
import imgHandKertas from '../../../public/assets/games/rock-paper-scissors/hand_kertas.png';
import imgHandGunting from '../../../public/assets/games/rock-paper-scissors/hand_gunting.png';
import imgIconRefresh from '../../../public/assets/games/rock-paper-scissors/icon_refresh.png';
import style from '../../../styles/games/rps.module.css';
import { insertGameScore } from '../../../actions/games';
import { Authentication } from '../../../actions/autentication';
import {
  playedGame,
  playerRank,
  totalGameByUser,
  totalPointByUser,
} from '../../../actions/fb_database';

function GameRPS() {
  const userLoginData = useSelector(
    (state) => state.userLoginReducer.loginUser,
  );
  const gameId = '-NJUNlGwqfwWDaJTykAR';
  // const uuid = localStorage.getItem('UID');
  const [show, setShow] = useState(false);
  const [currentGameInfo, setCurrentGameInfo] = useState({
    score: '0',
    resultGame: 'null',
  });

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  let uuid = null;
  const colorChose = '#C4C4C4';
  const colorUnchose = '#00000000';
  let haveResult = false;

  let textVs = null;
  let winner = null;
  let winnerText = null;

  const resultText = ['DRAW', 'PLAYER 1<br>WIN', 'COM<br>WIN'];
  let handP = [];
  let handCom = [];

  let hand = null;

  function cardHand(handChose, who) {
    for (let i = 1; i <= 3; i++) {
      hand[who][i].style.backgroundColor = colorUnchose;
    }

    if (handChose > 0) {
      hand[who][handChose].style.backgroundColor = colorChose;
    }
  }

  function reset() {
    haveResult = false;
    cardHand(0, 'com');
    cardHand(0, 'player');
    winner.style.display = 'none';
    textVs.style.display = 'block';
  }

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  async function press(youChose) {
    // console.log('Button has been pressed');
    if (haveResult) {
      return;
    }

    haveResult = true;
    const comChose = getRandomInt(1, 3);
    let whoWon = 0;
    const res = youChose - comChose;

    cardHand(comChose, 'com');
    cardHand(youChose, 'player');

    if (res !== 0) {
      if (res < 2 && res > -2) {
        if (youChose > comChose) {
          whoWon = 1;
        } else {
          whoWon = 2;
        }
      } else if (youChose > comChose) {
        whoWon = 2;
      } else {
        whoWon = 1;
      }
    }

    winnerText.innerHTML = resultText[whoWon];
    winner.style.display = 'block';
    textVs.style.display = 'none';

    if (whoWon === 1) {
      insertGameScore(gameId, uuid, 2);
      await totalGameByUser(uuid);
      await totalPointByUser(uuid);
      await playerRank(uuid);
      await playedGame(uuid);
      setCurrentGameInfo({
        score: 2,
        resultGame: 'PLAYER 1 WIN',
      });
      handleShow();
    } else if (whoWon === 2) {
      insertGameScore(gameId, uuid, -1);
      await totalGameByUser(uuid);
      await totalPointByUser(uuid);
      await playerRank(uuid);
      await playedGame(uuid);
      setCurrentGameInfo({
        score: -1,
        resultGame: 'COMP WIN',
      });
      handleShow();
    } else {
      insertGameScore(gameId, uuid, 0);
      await totalGameByUser(uuid);
      await totalPointByUser(uuid);
      await playerRank(uuid);
      await playedGame(uuid);
      setCurrentGameInfo({
        score: 0,
        resultGame: 'DRAW',
      });
      handleShow();
    }
  }

  useEffect(() => {
    Authentication();
    uuid = localStorage.getItem('UID');
    const handCom1 = document.getElementById('handCom1');
    const handCom2 = document.getElementById('handCom2');
    const handCom3 = document.getElementById('handCom3');

    const handP1 = document.getElementById('handP1');
    const handP2 = document.getElementById('handP2');
    const handP3 = document.getElementById('handP3');

    textVs = document.getElementById('textVs');
    winner = document.getElementById('winner');
    winnerText = document.getElementById('winnerText');

    handP = [null, handP1, handP2, handP3];
    handCom = [null, handCom1, handCom2, handCom3];

    hand = {
      player: handP,
      com: handCom,
    };
  }, [userLoginData, show, currentGameInfo]);

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Score And Result Game</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Score:
          {currentGameInfo.score}
        </Modal.Body>
        <Modal.Body>
          Result:
          {currentGameInfo.resultGame}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <div style={{ backgroundColor: '#9C835F' }}>
        <Navbar bgColor="#4A4A5C" />

        <div className="container">
          <div
            className="row text-center align-items-center justify-content-center"
            style={{ height: '100vh' }}
          >
            <div className="col-3 ">
              <div className="row ">
                <div className="col-12">
                  <h4 className="">
                    <strong>PLAYER 1</strong>
                  </h4>
                </div>
                <div className={`col-12 ${style.containerHandItems}`}>
                  <button
                    className={style.buttonRps}
                    type="button"
                    href="#"
                    onClick={() => {
                      press(1);
                    }}
                  >
                    <div className={`d-flex ${style.cardHand}`}>
                      <div id="handP1" className={style.cardHand}>
                        <Image src={imgHandBatu} className={style.imgHand} />
                      </div>
                    </div>
                  </button>
                </div>
                <div className={`col-12 ${style.containerHandItems}`}>
                  <button
                    className={style.buttonRps}
                    type="button"
                    onClick={() => {
                      press(2);
                    }}
                  >
                    <div className={`d-flex ${style.cardHand}`}>
                      <div id="handP2" className={style.cardHand}>
                        <Image src={imgHandKertas} className={style.imgHand} />
                      </div>
                    </div>
                  </button>
                </div>
                <div className={`col-12 ${style.containerHandItems}`}>
                  <button
                    className={style.buttonRps}
                    type="button"
                    onClick={() => {
                      press(3);
                    }}
                  >
                    <div className={`d-flex ${style.cardHand}`}>
                      <div id="handP3" className={`d-flex ${style.cardHand}`}>
                        <Image src={imgHandGunting} className={style.imgHand} />
                      </div>
                    </div>
                  </button>
                </div>
              </div>
            </div>
            <div
              className={`col-3 justify-content ${style.containerHandItems}`}
            >
              <h1 id="textVs" className={style.textVs}>
                <strong>VS</strong>
              </h1>
              <div id="winner" style={{ display: 'none' }}>
                <div className={`d-flex ${style.cardResult}`}>
                  <div className="d-flex justify-content-center">
                    <h4 id="winnerText" className="align-middle ">
                      WHO WIN?
                    </h4>
                  </div>
                </div>
              </div>

              <div className="position-absolute bottom-0 start-5 translate-middle-y">
                <button
                  className={style.buttonRps}
                  type="button"
                  onClick={() => {
                    reset();
                  }}
                >
                  <div className={`d-flex ${style.cardReset}`}>
                    <Image src={imgIconRefresh} className={style.imgReset} />
                  </div>
                </button>
              </div>
            </div>
            <div className="col-3">
              <div className="row">
                <div className="col-12">
                  <h4>
                    <strong>COM</strong>
                  </h4>
                </div>
                <div className={`col-12 ${style.containerHandItems}`}>
                  <div id="handCom1" className={style.cardHand}>
                    <Image src={imgHandBatu} className={style.imgHand} />
                  </div>
                </div>
                <div className={`col-12 ${style.containerHandItems}`}>
                  <div id="handCom2" className={style.cardHand}>
                    <Image src={imgHandKertas} className={style.imgHand} />
                  </div>
                </div>
                <div className={`col-12 ${style.containerHandItems}`}>
                  <div id="handCom3" className={style.cardHand}>
                    <Image src={imgHandGunting} className={style.imgHand} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default GameRPS;
