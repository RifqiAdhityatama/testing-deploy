import { Component } from 'react';
import {
  Form, Container, Row, Col,
} from 'react-bootstrap';

import '../utils/home/scripts';

import GameCard from '../components/Home/game_card';
import Slideshow from '../components/Home/slideshow';
import Footer from '../components/Layout/Footer/Footer';
import {
  getLeaderBoard,
  getVideoUrl,
  retrieveAllGames,
  retrieveAllSlideshow,
} from '../actions/games';
import Navbar from '../components/Layout/Nav/Navbar';
import Leaderboard from '../components/Home/leaderboard';
import VideoPlayer from '../components/Layout/VideoPlayer/VideoPlayer';
import { getVideoInfo } from '../actions/fb_database';

const style = {
  header: {
    backgroundImage: 'url(https://echamp.id/assets/img/header-bg.jpg)',
    paddingTop: 250,
    paddingBottom: 200,
    textAlign: 'center',
    color: '#fff',
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'scroll',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    fontFamily: 'Inter',
    fontStyle: 'normal',
  },
  header_card: {
    backgroundColor: '#3E4552',
    maxWidth: 800,
    padding: 50,
    borderRadius: 20,
    top: -80,
    position: 'relative',
  },
  slideshow: {
    height: 400,
  },
  section_leaderboard: {
    backgroundColor: '#353637',
  },
};

class Home extends Component {
  state = {
    gameSearch: '',
    gameList: [],
    slideshow: [],
    videoUrl: {
      url_video: 'http://res.cloudinary.com/dx4buq9vw/video/upload/v1671355898/assets/cjaznvuwseuxe2b4rcxg.mp4',
      video_title: 'valorant-trailer-duelist',
    },
  };

  handleSearchGame = async (event) => {
    await this.setState(() => ({
      gameSearch: event.target.value,
    }));
    this.componentDidMount();
  };

  async componentDidMount() {
    const url_video = await getVideoUrl();
    const dataGame = await retrieveAllGames();
    const dataSlideshow = await retrieveAllSlideshow();
    this.setState({
      gameList: dataGame,
      slideshow: dataSlideshow,
      videoUrl: url_video,
    });
    this.setState({
      videoUrl: url_video,
    });
    await getLeaderBoard();
    // console.log(this.state.videoUrl);
    // console.log(this.state.slideshow);
    // console.log(this.state.gameList);
    // console.log(url_video);
  }

  render() {
    return (
      <div style={{ backgroundColor: '#2B2D33', color: '#fff' }}>
        <Navbar bgColor="#4A4A5C" transparant="1" />

        <header className="" style={style.header}>
          <div className="container">
            <div className="masthead-subheading" style={{ fontSize: 25 }}>
              <b>FIND YOUR FAVORITE GAME</b>
            </div>
            <div
              className="masthead-heading text-uppercase"
              style={{ fontSize: 60 }}
            >
              <b>PLAY ONLINE</b>
            </div>
          </div>
        </header>

        <Container>
          <center>
            <div style={style.header_card}>
              <h2>ABOUT</h2>
              <span>
                play your favorite games online, no need to download and can be
                played for free, all the scores you get will be recorded and
                will be displayed if you are the best of everyone
              </span>
            </div>
          </center>
          <VideoPlayer
            source={this.state.videoUrl?.url_video}
          />
          <section className="page-section " id="game">
            <center>
              <div className="col-lg-9 col-sm-12">
                <Slideshow data={this.state.slideshow} />

                <Row xs={1} md={3} className="g-4 text-dark">
                  <Col />
                  <Col />
                  <Col>
                    <Form>
                      <Form.Group
                        className="mb-3 mt-3"
                        controlId="formBasicEmail"
                      >
                        <Form.Control
                          type="text"
                          placeholder="Search Game"
                          value={this.state.gameSearch}
                          onChange={this.handleSearchGame}
                        />
                      </Form.Group>
                    </Form>
                  </Col>
                </Row>

                <Row xs={1} md={1} className="g-4 py-3">
                  {this.state.gameList.map((data) => (
                    <GameCard
                      key={data.id}
                      game_id={data.id}
                      title={data.data.game_title}
                      description={data.data.game_description}
                      image={data.data.game_image}
                      url={data.data.game_url}
                    />
                  ))}
                </Row>
              </div>
            </center>
          </section>
        </Container>
        <section
          className="page-section  py-5"
          style={style.section_leaderboard}
          id="leaderboard"
        >
          <Leaderboard />
        </section>
        <Footer />
      </div>
    );
  }
}

export default Home;
