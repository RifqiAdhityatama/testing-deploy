import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { historyByUser } from '../../actions/fb_database';
import DynamicDocument from '../../components/DynamicPDF';
import Footer from '../../components/Layout/Footer/Footer';
import Navbar from '../../components/Layout/Nav/Navbar';
import style from '../../styles/pdf/pdf.module.css';

function PagePDF() {
  const [userGameHistory, setUserGameHistory] = useState([]);
  const userLoginData = useSelector(
    (state) => state.userLoginReducer.loginUser,
  );
  useEffect(() => {
    async function initData() {
      if (userLoginData[0]) {
        const userGameHistoryById = await historyByUser(
          userLoginData[0]?.data?.id_player,
        );
        setUserGameHistory(userGameHistoryById);
      }
    }
    initData();
  }, [userLoginData]);

  return (
    <div>
      <Navbar bgColor="#4A4A5C" />
      <div className={style.pdf}>
        <DynamicDocument />
      </div>
      <div className={style.footer}>
        <Footer />
      </div>
    </div>
  );
}

export default PagePDF;
